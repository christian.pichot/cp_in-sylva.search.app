import React from 'react';
import useStore from './useStore';
import * as actions from '../actions';

const initialState = {
  isLoading: false,
  status: 'INITIAL',
  roles: [],
  user: {},
  userDetails: {},
  error: false,
  fields: [],
  resources: [],
};
const store = useStore(React, initialState, actions);
export default store;
