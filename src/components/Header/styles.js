const headerStyle = {
  paddingTop: '10px',
  paddingBottom: '-6px',
  paddingRight: '10px',
  paddingLeft: '10px',
};

export default headerStyle;
