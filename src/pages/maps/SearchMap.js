import React, { useState, useEffect } from 'react';
import { Map, View } from 'ol';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import ImageLayer from 'ol/layer/Image';
import SourceOSM from 'ol/source/OSM';
import BingMaps from 'ol/source/BingMaps';
import { Vector as VectorSource } from 'ol/source';
import WMTS from 'ol/source/WMTS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';
import { getWidth } from 'ol/extent';
import {platformModifierKeyOnly} from 'ol/events/condition.js';
import ImageWMS from 'ol/source/ImageWMS';
import GeoJSON from 'ol/format/GeoJSON';
import {DragBox, Select} from 'ol/interaction.js';
import { Fill, Stroke, Style, Text, Icon } from 'ol/style';
import { Circle, Point, Polygon } from 'ol/geom';
import Feature from 'ol/Feature';
import * as proj from 'ol/proj';
import * as olColor from 'ol/color';
import {
  ScaleLine,
  MousePosition,
  OverviewMap,
  defaults as defaultControls,
} from 'ol/control';
import { toStringXY } from 'ol/coordinate';
import 'ol/ol.css';
import { EuiCheckbox, EuiComboBox, EuiPopover, EuiButtonEmpty, EuiText } from '@elastic/eui';
import { htmlIdGenerator } from '@elastic/eui/lib/services';
import { updateArrayElement } from '../../Utils.js';

const SearchMap = (props) => {

  const onChange = (selectedOptions) => { //selected map as filter
    const selectedOption = selectedOptions;
    setSelected(selectedOptions);
    var selectedMap = selectedOption[0].value;
    var deselectOption = document.getElementById('deselectCheckboxId');
    
    // a DragBox interaction used to select features
    const dragBox = new DragBox({
      condition: platformModifierKeyOnly,
    });
    
    var allMyLayers =  map.getLayers();
    //remove previous selected map used as filter
    for (let ifilter = 0; ifilter < options.length; ifilter++) {
      for (let i = 0; i < allMyLayers.getLength(); i++) {
        if ( options[ifilter].value == map.getLayers().item(i).get('name')){
          //window.alert('removed map:' + options[ifilter].value);
          map.removeLayer(map.getLayers().item(i));
        }
      }
    }
    
    var pointsSource = map.getLayers().item(getLayerIndex('query_results')).getSource();
    
    if(selectedMap == 'ResRequete'){
      toggleLayer('query_results');
      var polygonSource = pointsSource;
    }
    else{
      map.addLayer(maps4Filter[selectedMap]);
      var polygonSource = maps4Filter[selectedMap].get('source');
    }
        
    map.addInteraction(dragBox);

    // clear selection when drawing a new box and when clicking on the map
    dragBox.on('boxstart', function () {
      selectedFeatures.clear();
    });
          
    dragBox.on('boxend', function () {
      const boxExtent = dragBox.getGeometry().getExtent();
              
      // if the extent crosses the antimeridian process each world separately
      const worldExtent = map.getView().getProjection().getExtent();
      const worldWidth = getWidth(worldExtent);
      const startWorld = Math.floor((boxExtent[0] - worldExtent[0]) / worldWidth);
      const endWorld = Math.floor((boxExtent[2] - worldExtent[0]) / worldWidth);

      for (let world = startWorld; world <= endWorld; ++world) {
        const left = Math.max(boxExtent[0] - world * worldWidth, worldExtent[0]);
        const right = Math.min(boxExtent[2] - world * worldWidth, worldExtent[2]);
        const extent = [left, boxExtent[1], right, boxExtent[3]];

        //const boxFeatures = maps4Filter[selectedMap].get('source')
        const boxFeatures = polygonSource.getFeaturesInExtent(extent)
        .filter(
        (feature) =>
        !selectedFeatures.getArray().includes(feature) &&
        feature.getGeometry().intersectsExtent(extent),
        );

        var pointsFeatures = map.getLayers().item(getLayerIndex('query_results')).getSource().getFeatures();
        var selectedPointsSource = map.getLayers().item(getLayerIndex('selectedPointsSource')).getSource();
  
        for (let polygone = 0; polygone < boxFeatures.length; polygone++) {
          var polygonGeometry = boxFeatures[polygone].getGeometry();
          if (!deselectOption.checked){
            for (let point = 0; point < pointsFeatures.length; point++) {
              var pointGeom = pointsFeatures[point].getGeometry();
              var pointName = pointsFeatures[point].get('nom');
              var coords = pointGeom.getCenter();
              if (polygonGeometry.intersectsCoordinate(coords)){
                if (!getFeatureNames(selectedPointsSource).includes(pointName)){
                    var selectedPointFeature = new Feature(new Circle(coords,5000));
                    selectedPointFeature.set('nom', pointName);
                    selectedPointsSource.addFeature(selectedPointFeature);
                }
              }
            }
          }
          else { //remove selected features
            var selectedPointsFeatures = selectedPointsSource.getFeatures();
            if(selectedPointsFeatures.length > 0 ) {
              for (let point = 0; point < selectedPointsFeatures.length; point++) {
                var pointGeom = selectedPointsFeatures[point].getGeometry();
                var coords = pointGeom.getCenter();
                if (polygonGeometry.intersectsCoordinate(coords)){
                  selectedPointsSource.removeFeature(selectedPointsFeatures[point]);                                      
                }
              }
            }
          }
        }
        selectedFeatures.extend(boxFeatures);
      }
    });
  };

  const selectedStyle = new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.6)',
    }),
    stroke: new Stroke({
      color: 'rgba(255, 255, 255, 0.7)',
      width: 2,
    }),
  });

  // a normal select interaction to handle click
  const select = new Select({
    style: function (feature) {
      //const color = feature.get('COLOR_BIO') || '#eeeeee';
      const color = 'rgba(255, 255, 255, 0.4)';
      selectedStyle.getFill().setColor(color);
      return selectedStyle;
    },
  });

  const selectedFeatures = select.getFeatures();
  const selectedPointsFeatures = select.getFeatures();

  selectedFeatures.on(['add', 'remove'], function () {
    const names = selectedFeatures.getArray().map((feature) => {
      //return feature.get('ECO_NAME');
      return feature.get('nom');
    });
    if (names.length > 0) {
      console.log(names.join(', '));
    }
    else {
    console.log('None');
    }
  });

  selectedPointsFeatures.on(['add', 'remove'], function () {
    const names = getFeatureNames(selectedPointsSource);
    if (names.length > 0) {
      //infoBoxPoints.innerHTML = names.join(', ');
    } else {
      //infoBoxPoints.innerHTML = 'None';
    }
  });

  function getFeatureNames(source) {
    var featureNames = [];
    source.getFeatures().forEach(function (feature) {
        var name = feature.get('nom');
        if (name) {
            featureNames.push(name);
        }
    });
    return featureNames;
  }
  const styles = {
    tableStyle: {
      border: '1px solid black',
    },
    trStyle: {
      verticalAlign: 'top'
    },
    'Point': new Style({
      image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: 'https://openlayers.org/en/v3.20.1/examples/data/icon.png',
      }),
    }),
    'MultiPoint': new Style({
        image: new Icon({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'https://openlayers.org/en/v3.20.1/examples/data/icon.png'
        })
    }),
    Circle: new Style({
      stroke: new Stroke({
        color: 'black',
        width: 2,
      }),
      //radius: 1000,
      fill: new Fill({
        color: 'rgba(0,0,255,0.3)',
      }),
    }),
    /* 'LineString': new Style({
            stroke: new Stroke({
                color: 'green',
                width: 1,
            }),
        }),
        'MultiLineString': new Style({
            stroke: new Stroke({
                color: 'green',
                width: 1,
            }),
        }),
        'MultiPolygon': new Style({
            stroke: new Stroke({
                color: 'yellow',
                width: 1,
            }),
            fill: new Fill({
                color: 'rgba(255, 255, 0, 0.1)',
            }),
        }),
        'Polygon': new Style({
            stroke: new Stroke({
                color: 'blue',
                lineDash: [4],
                width: 3,
            }),
            fill: new Fill({
                color: 'rgba(0, 0, 255, 0.1)',
            }),
        }),
        'GeometryCollection': new Style({
            stroke: new Stroke({
                color: 'magenta',
                width: 2,
            }),
            fill: new Fill({
                color: 'magenta',
            }),
            image: new CircleStyle({
                radius: 10,
                fill: null,
                stroke: new Stroke({
                    color: 'magenta',
                }),
            }),
        }),*/
    mapContainer: {
      height: '80vh',
      width: '60vw',
    },
    layerTree: {
      cursor: 'pointer',
    },
  };
  const source = new SourceOSM();
  const overviewMapControl = new OverviewMap({
    layers: [
      new TileLayer({
        source: source,
      }),
    ],
  });
  const [center, setCenter] = useState(proj.fromLonLat([2.5, 46.5]));
  //const [center, setCenter] = useState(proj.fromLonLat([3.5, 44.5]));
  const [zoom, setZoom] = useState(6);
  const styleFunction = function (feature) {
    return styles[feature.getGeometry().getType()];
  };

  //maps used to filter results
  const options = [
    { label: 'Résultats de la requête', value: 'ResRequete' },
    //{ label: 'SylvoEcoRégions', value: 'SylvoEcoRegions' },
    { label: 'Régions', value: 'Regions' },
    { label: 'Départements', value: 'Departements' },
  ];
  const [selectedOptions, setSelected] = useState([options[0]]);

  const regions_IGN = new VectorSource({
    url: 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ADMINEXPRESS-COG-CARTO.LATEST:region&outputFormat=application/json',
    format: new GeoJSON(),
    });

  const departements_IGN = new VectorSource({
    url: 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ADMINEXPRESS-COG-CARTO.LATEST:departement&outputFormat=application/json',
    format: new GeoJSON(),
    });

  const sylvoecoregions_IGN = new VectorSource({
    url: 'https://wxs.ign.fr/environnement/geoportail/wmts?VERSION=2.0.0&request=GetFeature&typeName=LANDCOVER.SYLVOECOREGIONS&outputFormat=application/json',
    format: new GeoJSON(),
    });

  const resultPointsSource = new VectorSource({});
  const resultPointsSource4BG = new VectorSource({});
  var selectedPointsSource = new VectorSource({});

  var maps4Filter = {
    'ResRequete': new VectorLayer({
       name: 'ResRequete',
       source: resultPointsSource,
       style: styleFunction,
    }),
    'selectedPointsLayer': new VectorLayer({
      name: 'selectedPointsSource',
      source: selectedPointsSource,
      style: styleFunction,
    }),
    'Regions': new VectorLayer({
      name: 'Regions',
      source: regions_IGN,
      //background: '#1a2b39',
      opacity: 0.5,
      style: function (feature) {
        ////const color = feature.get('COLOR_BIO') || '#eeeeee';
        ////const color = '#eeeeee';
        var stroke = new Stroke({
          width : 2,
          lineDash: [4],
          color : 'black'
        });
        var colorArray = olColor.asArray('grey').slice();
        colorArray[3] = 0.5;
        style.getFill().setColor(colorArray);
        style.setStroke(stroke);  
        return style;
      },
    }),
    'Departements': new VectorLayer({
      name: 'Departements',
      source: departements_IGN,
      //background: '#1a2b39',
      opacity: 0.5,
      style: function (feature) {
        ////const color = feature.get('COLOR_BIO') || '#eeeeee';
        ////const color = '#eeeeee';
        var stroke = new Stroke({
          width : 2,
          lineDash: [4],
          color : 'black'
        });
        var colorArray = olColor.asArray('grey').slice();
        colorArray[3] = 0.5;
        style.getFill().setColor(colorArray);
        style.setStroke(stroke);  
        return style;
      },
    }),
  }

  const style = new Style({
    fill: new Fill({
      color: '#eeeeee',
    }),
  });
 
  const resolutions = [];
  const matrixIds = [];
  const proj3857 = proj.get('EPSG:3857');
  const maxResolution = getWidth(proj3857.getExtent()) / 256;

  for (let i = 0; i < 20; i++) {
    matrixIds[i] = i.toString();
    resolutions[i] = maxResolution / Math.pow(2, i);
  }

  const tileGrid = new WMTSTileGrid({
    origin: [-20037508, 20037508],
    resolutions: resolutions,
    matrixIds: matrixIds,
  });

  const [mapLayers, setMapLayers] = useState([
    new TileLayer({
      name: 'osm-layer',
      source: source,
    }),
    /* Bing Aerial */
    new TileLayer({
      name: 'Bing Aerial',
      preload: Infinity,
      source: new BingMaps({
        key: 'AtdZQap9X-lowJjvdPhTgr1BctJuGGm-ZoVw9wO6dHt1VDURjRKEkssetwOe31Xt',
        imagerySet: 'Aerial',
      }),
      //visible: false
    }),
    new TileLayer({
      name: 'IGN',
      source: new WMTS({
        url: 'https://wxs.ign.fr/choisirgeoportail/geoportail/wmts',
        layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
        matrixSet: 'PM',
        format: 'image/png',
        projection: 'EPSG:3857',
        tileGrid: tileGrid,
        style: 'normal',
        attributions:
          '<a href="https://www.ign.fr/" target="_blank">' + 
          '<img src="https://www.ign.fr/files/default/styles/thumbnail/public/2020-06/logoIGN_300x200.png?itok=V80_0fm-" title="Institut national de l\'' +
          //'<img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" title="Institut national de l\'' +
          'information géographique et forestière" alt="IGN"></a>',
      }),
    }),
    new TileLayer({
      name: 'SylvoEcoregions',
      source: new WMTS({
        url: 'https://data.geopf.fr/wmts?',
        layer: 'LANDCOVER.SYLVOECOREGIONS',
        matrixSet: 'PM',
        format: 'image/png',
        projection: 'EPSG:3857',
        tileGrid: tileGrid,
        style: 'normal',
        attributions:
        '<a href="https://www.ign.fr/" target="_blank">' + 
        '<img src="https://www.ign.fr/files/default/styles/thumbnail/public/2020-06/logoIGN_300x200.png?itok=V80_0fm-" title="Institut national de l\'' +
        'information géographique et forestière" alt="IGN"></a>',
      }),
    }),
    new VectorLayer({
      name: 'query_results',
      source: resultPointsSource4BG,
    }),
    maps4Filter['selectedPointsLayer'],
  ]);

  const [mapLayersVisibility, setMapLayersVisibility] = useState(
    new Array(mapLayers.length).fill(true)
  );

  const [deselectChecked, setDeselectChecked] = useState();
  const handleDeselectCheckboxChange = (event) => {
    setDeselectChecked(!deselectChecked);
  };


  // const posGreenwich = proj.fromLonLat([0, 51.47]);
  // set initial map objects
  const view = new View({
    center: center,
    zoom: zoom,
  });

  //list of selected results
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const onButtonClick = () =>{
    setIsPopoverOpen((isPopoverOpen) => !isPopoverOpen);    
   }
  const closePopover = () => setIsPopoverOpen(false);

  const button = (
    <EuiButtonEmpty
      iconType="documentation"
      iconSide="right"
      onClick={onButtonClick}
    >
      Liste des résultats sélectionnés
    </EuiButtonEmpty>
  );

  const [map] = useState(
    new Map({
      target: null,
      layers: mapLayers,
      controls: defaultControls().extend([
        new MousePosition({
          projection: 'EPSG:4326',
        }),
        new ScaleLine(),
        overviewMapControl,
      ]),
      view: view,
    })
  );

  const processData = (props) => {
    if (props.searchResults) {
      props.searchResults.forEach((result) => {
        if (
          result.experimental_site.geo_point &&
          result.experimental_site.geo_point.longitude &&
          result.experimental_site.geo_point.latitude
        ) {
          const coord = [
            result.experimental_site.geo_point.longitude,
            result.experimental_site.geo_point.latitude,
          ];
          const pointIdentifier = result.resource.identifier + "--" + result.context.related_experimental_network_title
          var pointFeature = new Feature(new Circle(proj.fromLonLat(coord),2000));
          pointFeature.set('nom', pointIdentifier);
          resultPointsSource4BG.addFeature(pointFeature);
        }
      });
    }
  };

  // useEffect Hooks
  // [] = component did mount
  // set the initial map targets
  useEffect(() => {
    map.setTarget('map');
    map.on('moveend', () => {
      setCenter(map.getView().getCenter());
      setZoom(map.getView().getZoom());
    });
    map.addInteraction(select);
    map.getView().animate({ zoom: zoom }, { center: center }, { duration: 2000 });
    processData(props);
    // clean up upon component unmount
    /* return () => {
            map.setTarget(null);
        }; */
  }, [props]);

  const getLayerIndex = (name) => {
    let index = 0;
    mapLayers.forEach((layer) => {
      if (layer.get('name') === name) {
        index = mapLayers.indexOf(layer);
      }
    });
    return index;
  };

  const toggleLayer = (name) => {
    let updatedLayers = mapLayers;
    const layerIndex = getLayerIndex(name);
    // let updatedLayer = updatedLayers[getLayerIndex(name)]
    setMapLayersVisibility(
      updateArrayElement(
        mapLayersVisibility,
        layerIndex,
        !mapLayersVisibility[layerIndex]
      )
    );
    updatedLayers[layerIndex].setVisible(!updatedLayers[layerIndex].getVisible());
    setMapLayers(updatedLayers);
  };

  // helpers
  /* const btnAction = () => {
        // when button is clicked, recentre map
        // this does not work :(
        setCenter(posGreenwich);
        setZoom(6);
    }; */
  // render
  return (
    <div>
      <div id="map" style={styles.mapContainer}></div>
      <div id="layertree">
        <br />
        <h5>Couches géographiques</h5>
        <table style={styles.tableStyle}>
          <thead>
            <tr style={styles.trStyle}>
                <th>Fonds de carte</th>
                <th>Filtres géo-thématiques</th>
            </tr>
          </thead>
          <tbody>
            <tr style={styles.trStyle}>
              <td>
                <ul>
                  <li>
                    <EuiCheckbox
                      id={htmlIdGenerator()()}
                      label="Query result"
                      checked={mapLayersVisibility[getLayerIndex('query_results')]}
                      onChange={(e) => toggleLayer('query_results')}
                    />
                  </li>
                  <li>
                    <EuiCheckbox
                      id={htmlIdGenerator()()}
                      label="Open Street Map"
                      checked={mapLayersVisibility[getLayerIndex('osm-layer')]}
                      onChange={(e) => toggleLayer('osm-layer')}
                    />
                  </li>
                  <li>
                    <EuiCheckbox
                      id={htmlIdGenerator()()}
                      label="Bing Aerial"
                      checked={mapLayersVisibility[getLayerIndex('Bing Aerial')]}
                      onChange={(e) => toggleLayer('Bing Aerial')}
                    />
                  </li>
                  <li>
                    <EuiCheckbox
                      id={htmlIdGenerator()()}
                      label="PLAN IGN"
                      checked={mapLayersVisibility[getLayerIndex('IGN')]}
                      onChange={(e) => toggleLayer('IGN')}
                    />
                  </li>
                  <li>
                    <EuiCheckbox
                      id={htmlIdGenerator()()}
                      label="SylvoEcorégions"
                      checked={mapLayersVisibility[getLayerIndex('SylvoEcoregions')]}
                      onChange={(e) => toggleLayer('SylvoEcoregions')}
                    />
                  </li>
                </ul>
              </td>
              <td>
                <ul>
                  <input
                    id="deselectCheckboxId"
                    type="checkbox"
                    checked={deselectChecked}
                    onChange={handleDeselectCheckboxChange}
                    /><i>Délectionner</i>
                  <EuiComboBox
                    aria-label="Accessible screen reader label"
                    placeholder="Select a single option"
                    singleSelection={{ asPlainText: true }}
                    options={options}
                    selectedOptions={selectedOptions}
                    onChange={onChange}
                  />
                  <br/>
                  <EuiPopover
                    button={button}
                    isOpen={isPopoverOpen}
                    closePopover={closePopover}
                  >
                    <EuiText style={{ width: 300 }}>
                      <p>{getFeatureNames(map.getLayers().item(getLayerIndex('selectedPointsSource')).getSource()).toString()}</p>
                    </EuiText>
                  </EuiPopover>
                </ul>
              </td>
              <td>
                <div id="info">&nbsp;</div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      {/*<div
                style={styles.bluecircle}
                ref={overlayRef}
                id="overlay"
                title="overlay"
            />*/}
      {/*<button
                style={{
                    position: "absolute",
                    right: 10,
                    top: 10,
                    backgroundColor: "white"
                }}
                onClick={() => {
                    btnAction();
                }}
            >
                CLICK
            </button>*/}
    </div>
  );
};

export default SearchMap;